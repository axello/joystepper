# joystepper

A Swift for Arduino project which connects a joystick to two stepper motor drivers. To play around with and/or test a CNC or 3D printer.


Basically connecting this:
![arduino joystick](https://imgaz1.staticbg.com/thumb/view/oaupload/banggood/images/34/43/9d2786d9-34e1-4fdf-8256-0e14119f65bf.jpg)

with this:

![](https://imgaz1.staticbg.com/thumb/view/oaupload/banggood/images/F7/64/548cd824-2ee9-47cd-9f5f-829e238c748f.JPG)

using this:

![](https://imgaz2.staticbg.com/thumb/view/oaupload/ser1/banggood/images/AE/B9/868202eb-b8cc-48a1-82b6-0753578faf23.JPG)


*images from BangGood.com*