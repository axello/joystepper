import AVR

/* struct which handles one joystick:
    x
    y
    button
*/

struct Joystick {
    let xPin: Pin
    let yPin: Pin
    let buttonPin: Pin
    private var midX: Int16 = 512
    private var midY: Int16 = 512

    public var x: UInt16 = 0
    public var y: UInt16 = 0
    var button: Bool {
        get {
            return digitalRead(pin: buttonPin)
        }
    }

    var xDirection: Int16 {
        get {
            return Int16(x) - midX
        }
    }
    var yDirection: Int16 {
        get {
            return Int16(y) - midY
        }
    }

    init(xPin: Pin, yPin: Pin, buttonPin: Pin) {
        self.xPin = xPin
        self.yPin = yPin
        self.buttonPin = buttonPin
               pinMode(pin: buttonPin, mode: INPUT)

    }

    mutating func calibrate(midX: UInt16, midY: UInt16) {
        self.midX = Int16(midX)
        self.midY = Int16(midY)
    }

}
