//------------------------------------------------------------------------------
//
// JoyCNC.swift4a
// Swift For Arduino
//
// Created by Axel Roest on 08/26/2020.
// Copyright © 2020 Axel Roest. All rights reserved.
//
// NOTE: Modifications to the "Libraries:" comment line below will affect the build.
// Libraries:
//------------------------------------------------------------------------------

import AVR
typealias IntegerLiteralType = Pin

//------------------------------------------------------------------------------
// Setup / Functions
//------------------------------------------------------------------------------

let joystickX: Pin = A4
let joystickY: Pin = A5
var xmotor = Stepmotor(directionPin: D5, clockPin: D2)
var ymotor = Stepmotor(directionPin: D6, clockPin: D3)

var joystick = Joystick(xPin: joystickX, yPin: joystickY, buttonPin: A0)
let callbackMs: UInt16 = 50
var nextTicks: UInt32 = 0
var serialDelay: UInt16 = 1000

func setup() {
    // determine midways of joystick
    // assume joystick is untouched
    calibrateJoystick()
    setupClock()
    SetupSerial()
}

func setupClock() {
    executeAsync(after: callbackMs, repeats: true, callback: updateClocks)
}

func updateClocks() {
    xmotor.updateClock()
}

func calibrateJoystick() {
    let midX: UInt16 = analogReadSync(pin: joystickX)
    let midY: UInt16 = analogReadSync(pin: joystickY)
    joystick.calibrate(midX: midX, midY: midY)
}

func readJoystick() {
    analogReadAsync(pin: joystickX, callback: { value in
        joystick.x = value
    })
    analogReadAsync(pin: joystickY, callback: { value in
        joystick.y = value
    })
}

func handleSteering() {
    var messagex = ""
    var messagey = ""
    if joystick.xDirection < 0 {
        xmotor.changeDirection(false)
        xmotor.changeSpeed(1)
        messagex = "<-- "
    } else if joystick.xDirection > 0 {
        xmotor.changeDirection(true)
        xmotor.changeSpeed(1)
        messagex = "--> "
    } else if joystick.xDirection == 0 {
        xmotor.changeSpeed(0)
        messagex = "- "
    }

    if joystick.yDirection < 0 {
        ymotor.changeDirection(false)
        ymotor.changeSpeed(1)
        messagey = "up "
    } else if joystick.yDirection > 0 {
        ymotor.changeDirection(true)
        ymotor.changeSpeed(1)
        messagey = "down "
    } else if joystick.yDirection == 0 {
        ymotor.changeSpeed(0)
        messagey = "| "
    }

    if ticks() > nextTicks {
        nextTicks = ticks() + UInt32(serialDelay)
        print(message: messagex, addNewline: false)
        print(message: messagey, addNewline: true)
        if joystick.button {
            print(message: "BUTTON ", addNewline: false)
        }
    
    }


}
//------------------------------------------------------------------------------
// Start
//------------------------------------------------------------------------------
setup()
//------------------------------------------------------------------------------
// Main Loop
//------------------------------------------------------------------------------
while(true) {
    readJoystick()
    handleSteering()
    // Insert code to be run in main loop here.
}

//------------------------------------------------------------------------------
