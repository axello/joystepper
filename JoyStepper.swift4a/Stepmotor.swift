import AVR

/*
 struct which handles one stepmotor, to drive a stepmotor driver IC, not the motor coils directly!
    direction: left/right value
    clock: do one step
*/

struct Stepmotor {
    private var speed: UInt8 = 0
    private var direction: Pin
    var clock: Pin
    @Volatile var level: Bool = LOW

    init(directionPin: Pin, clockPin: Pin) {
        direction = directionPin
        clock = clockPin
        pinMode(pin: direction, mode: OUTPUT)
        pinMode(pin: clock, mode: OUTPUT)
    }

    mutating func updateClock() {
        if speed > 0 {
            level.toggle()
            digitalWrite(pin: clock, value: level)
            digitalWrite(pin: D13, value: level)
        }
    }

    func changeDirection(_ dir: Bool) {
        digitalWrite(pin: direction, value: dir)
    }

    mutating func changeSpeed(_ speed: UInt8) {
        self.speed = speed
    }
}
